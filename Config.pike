import ".";

bool secure = true;

class Section
{
    private string name;
    private mapping(string:mixed) variables = ([ ]);
    private mapping(string:Variable) definitions = ([ ]);
    private bool secure = true;

    void add_variable(string key, string description,
                        mixed type, void|mixed def)
    {
        if (has_index(variables, key) && secure)
            error("%s->%s is already defined.\n", name, key);

        definitions[key] = type(key, description);
        if (def && definitions[key]->validate(def))
            variables[key] = def;
        else
            variables[key] = UNDEFINED;
    }

    mixed `[]=(string key, mixed value)
    {
        if (has_index(definitions, key) && definitions[key]->validate(value))
            variables[key] = value;
        else
            error("Unknown variable or value is not valid.\n");

        return value;
    }

    mixed `[](string key)
    {
        if (!has_index(definitions, key))
        {
            if (secure)
                error("Unknown variable.\n");
            else
                return UNDEFINED;
        }

        return variables[key];
    }

    void create(string n, void|bool _secure)
    {
        name = n;
        secure = (undefinedp(_secure)) ? true : _secure;
    }

    string _sprintf()
    {
        return sprintf("%O", variables);
    }

    string encode_json()
    {
        return Standards.JSON.encode(variables, Standards.JSON.HUMAN_READABLE);
    }
}

private string cfg_file;
private mapping(string:Section) sections = ([ ]);

void create(string cfg, void|bool _secure)
{
    cfg_file = cfg;
    secure = (undefinedp(_secure)) ? true : _secure;
}

void add_variable(string section, string name, string description,
                    mixed type, void|mixed def)
{
    if (!sections[section])
        sections[section] = Section(section, secure);

    sections[section]->add_variable(name, description, type, def);
}

int read()
{
    if (!Stdio.exist(cfg_file))
        return 0;

    string conf = Stdio.read_file(cfg_file);

    if (!conf || !strlen(conf))
    {
        werror("Config file is empty or unreadable.\n");
        return 1;
    }

    int err = Standards.JSON.validate_utf8(conf);
    if (err > -1)
    {
        werror("Config is not a valid JSON utf-8 document. Error at: %d\n", err);
        return 2;
    }

    foreach (Standards.JSON.decode_utf8(conf);
        string section;
        mapping(string:mixed) sdata)
    {
        if (has_index(sections, section))
        {
            foreach (sdata; string key; mixed val)
            {
                mixed e = catch {
                    sections[section][key] = val;
                };

                if (e)
                {
                    werror("Unknown variable in config section %s: %s = %O\n",
                        section, key, val);
                    return 3;
                }
            }
        }
        else
        {
            werror("Unknown section in config: %s\n", section);
            return 4;
        }
    }

    return 0;
}

bool write()
{
    mixed err = catch{
        Stdio.write_file(cfg_file, string_to_utf8(
            Standards.JSON.encode(sections, Standards.JSON.HUMAN_READABLE)
        ), 0644);
    };

    if (err)
        return false;

    return true;
}

mixed `[]=(string key, mixed value)
{
    error("Setting sections is forbidden.\n");
}

mixed `[](string key)
{
    if (!has_index(sections, key))
    {
        if (secure)
            error(sprintf("Unknown section %s.\n", key));
        else
            return UNDEFINED;
    }

    return sections[key];
}

string _sprintf()
{
    return sprintf("%O", sections);
}

