class Variable {
    string name;
    string description;

    bool validate(mixed v) { return true; }
}

class Bool {
    inherit Variable;
    bool validate(bool v) { return intp(v) && (<false, true>)[v]; }
}

class Int {
    inherit Variable;
    bool validate(int v) { return intp(v) || v == Val.null; }
}

class Float {
    inherit Variable;
    bool validate(float v) { return floatp(v) || v == Val.null; }
}

class String {
    inherit Variable;
    bool validate(string v) { return stringp(v) || v == Val.null; }
}

class Array {
    inherit Variable;
    bool validate(array v) { return arrayp(v) || v == Val.null; }
}

class Mapping {
    inherit Variable;
    bool validate(mapping v) { return mappingp(v) || v == Val.null; }
}

class Multiset {
    inherit Variable;
    bool validate(multiset v) { return multisetp(v) || v == Val.null; }
}
